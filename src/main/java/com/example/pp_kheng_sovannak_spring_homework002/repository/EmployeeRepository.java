package com.example.pp_kheng_sovannak_spring_homework002.repository;

import com.example.pp_kheng_sovannak_spring_homework002.model.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Employee addEmployee(Employee employee){
        entityManager.persist(employee);
        return employee;
    }

    @Transactional
    public Employee updateEmployee(Long id){
        Employee employee = findEmployee(id);
        if(employee != null){
            entityManager.detach(employee);
            employee.setFirstName("John");
            employee.setLastName("Cina");
            employee.setEmail("cina.jonh@gmail.com");
            entityManager.merge(employee);
            entityManager.flush();
        }
        return employee;
    }

    public Employee findEmployee(Long id){
        return entityManager.find(Employee.class, id);
    }

    @Transactional
    public Employee removeEmployee(Long id){
        Employee employee = findEmployee(id);
        if(employee != null){
            entityManager.remove(employee);
        }
        return employee;
    }

}
