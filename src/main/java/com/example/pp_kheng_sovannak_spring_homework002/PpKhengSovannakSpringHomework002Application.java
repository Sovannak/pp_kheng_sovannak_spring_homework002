package com.example.pp_kheng_sovannak_spring_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpKhengSovannakSpringHomework002Application {

    public static void main(String[] args) {
        SpringApplication.run(PpKhengSovannakSpringHomework002Application.class, args);
    }

}
