package com.example.pp_kheng_sovannak_spring_homework002.controller;

import com.example.pp_kheng_sovannak_spring_homework002.model.Employee;
import com.example.pp_kheng_sovannak_spring_homework002.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("api/v1/employees")
@AllArgsConstructor
public class EmployeeController {

    private EmployeeRepository employeeRepository;

    @PostMapping("")
    public ResponseEntity<Employee> addEmployee(){
        Employee employee = new Employee(null, "Kheng", "Sovannak", "sovannak.kheng0309@gmail.com", new Date(), "hello");
        return ResponseEntity.ok().body(employeeRepository.addEmployee(employee));
    }

    @GetMapping("")
    public ResponseEntity<Employee> findEmployee(){
        return ResponseEntity.ok().body(employeeRepository.findEmployee(1L));
    }

    @DeleteMapping("")
    public ResponseEntity<Employee> removeEmployee(){
        return ResponseEntity.ok().body(employeeRepository.removeEmployee(1L));
    }

    @PutMapping("")
    public ResponseEntity<Employee> updateEmployee(){
        return ResponseEntity.ok().body(employeeRepository.updateEmployee(1L));
    }
}
